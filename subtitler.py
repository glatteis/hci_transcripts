import os
import glob
import webvtt
import sys
import requests
from textwrap import wrap

def vtt_to_text(file_name):
    vtt = webvtt.read(file_name)
    transcript = ""

    lines = []
    for line in vtt:
        lines.extend(line.text.strip().splitlines())

    previous = None
    for line in lines:
        if line == previous:
            continue
        transcript += " " + line
        previous = line
    
    output = ""

    punctuator = "http://bark.phon.ioc.ee/punctuator"
    for chunk in wrap(transcript, 5000):
        response = requests.post(url = punctuator, params = {"text": chunk})

        if response.status_code != 200:
            print(response)
            pass
            
        output += response.text


    return output

# download all subtitle files
os.system("youtube-dl --write-sub --write-auto-sub --sub-lang en --skip-download " +
    " --output \"./files/%(title)s.%(ext)s\" \"https://www.youtube.com/watch?v=6_laAvyFps8&list=PLABNOB9DXl_1m2dEAhVyZMtGQy-fa_qh-\"")

for f in glob.glob("files/*.vtt"):
    out_text = vtt_to_text(f)
    open(f.replace(".vtt", ".txt"), "w").write(out_text)


markdown_file = "# The Transcripts\n\n"
files = glob.glob("files/*.txt")
files.sort(key=lambda s: list(map(int, s.split(" ")[4].split('.'))))
for f in files:
    markdown_file += "## " + f.replace(".en.txt", "") + "\n"
    markdown_file += open(f).read() + "\n\n"
    print(f)


open("README.md", "w").write(markdown_file)

os.system("pandoc --from=markdown --to=html5 --output=transcripts.pdf --toc README.md")
